import '../models/model_expense.dart';

final List<ModelExpense> dataRegisteredExpenses = <ModelExpense>[
  ModelExpense(
    title: 'Flutter course',
    amount: 12.99,
    date: DateTime.now(),
    category: ModelExpenseCategory.work,
  ),
  ModelExpense(
    title: 'Fly to Armenia',
    amount: 99.99,
    date: DateTime.now(),
    category: ModelExpenseCategory.travel,
  ),
];
