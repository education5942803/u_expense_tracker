import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:uuid/uuid.dart';

const uuid = Uuid();
final formatter = DateFormat.yMd();

enum ModelExpenseCategory { food, travel, leisure, work }

const Map<ModelExpenseCategory, IconData> categoryIcons = {
  ModelExpenseCategory.food: Icons.lunch_dining,
  ModelExpenseCategory.travel: Icons.flight,
  ModelExpenseCategory.leisure: Icons.movie,
  ModelExpenseCategory.work: Icons.work,
};

class ModelExpense {
  ModelExpense({
    required this.title,
    required this.amount,
    required this.date,
    required this.category,
  }) : id = uuid.v4();

  final String id;
  final String title;
  final double amount;
  final DateTime date;
  final ModelExpenseCategory category;

  String get formattedDate => formatter.format(date);
}

class ModelExpenseBucket {
  const ModelExpenseBucket({
    required this.category,
    required this.expenses,
  });

  ModelExpenseBucket.forCategory(List<ModelExpense> allExpenses, this.category)
      : expenses = allExpenses
            .where((expense) => expense.category == category)
            .toList();

  final ModelExpenseCategory category;
  final List<ModelExpense> expenses;

  double get totalExpenses {
    double sum = 0;

    for (final ModelExpense expense in expenses) {
      sum += expense.amount;
    }

    return sum;
  }
}
