import 'package:flutter/material.dart';

import 'data/data_registered_expenses.dart';
import 'models/model_expense.dart';
import 'widgets/chart/chart.dart';
import 'widgets/expenses_list/expenses_list.dart';
import 'widgets/new_expense.dart';

class UExpenseTracker extends StatefulWidget {
  const UExpenseTracker({super.key});

  @override
  State<UExpenseTracker> createState() => _UExpenseTrackerState();
}

class _UExpenseTrackerState extends State<UExpenseTracker> {
  void _addNewExpense(ModelExpense newExpense) {
    setState(() => dataRegisteredExpenses.add(newExpense));
  }

  void _removeExpense(ModelExpense expense) {
    final int expenseIndex = dataRegisteredExpenses.indexOf(expense);

    setState(() => dataRegisteredExpenses.remove(expense));

    ScaffoldMessenger.of(context).clearSnackBars();
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        duration: const Duration(seconds: 3),
        content: const Text('Expense was deleted'),
        action: SnackBarAction(
          label: 'Undo',
          onPressed: () {
            setState(() {
              dataRegisteredExpenses.insert(expenseIndex, expense);
            });
          },
        ),
      ),
    );
  }

  void _openAddExpenseOverlay() {
    showModalBottomSheet(
      useSafeArea: true,
      isScrollControlled: true,
      context: context,
      constraints: const BoxConstraints(maxWidth: double.infinity),
      builder: (ctx) => NewExpense(onAddExpense: _addNewExpense),
    );
  }

  @override
  Widget build(BuildContext context) {
    final double maxDeviceWidth = MediaQuery.of(context).size.width;

    Widget mainContent = const Center(
      child: Text('No expenses here. Add something!'),
    );

    if (dataRegisteredExpenses.isNotEmpty) {
      mainContent = ExpensesList(
        expenses: dataRegisteredExpenses,
        onRemoveExpense: _removeExpense,
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text('U Expense Tracker'),
        actions: <Widget>[
          ElevatedButton.icon(
            onPressed: _openAddExpenseOverlay,
            icon: const Icon(Icons.add),
            label: const Text('Add expense'),
          ),
        ],
      ),
      body: maxDeviceWidth < 600
          ? Column(
              children: <Widget>[
                Chart(expenses: dataRegisteredExpenses),
                Expanded(child: mainContent),
              ],
            )
          : Row(
              children: <Widget>[
                Expanded(
                  child: Chart(expenses: dataRegisteredExpenses),
                ),
                Expanded(child: mainContent),
              ],
            ),
    );
  }
}
