import 'package:flutter/material.dart';

import '../../models/model_expense.dart';

class ExpenseItem extends StatelessWidget {
  const ExpenseItem(this.expense, {super.key});

  final ModelExpense expense;

  @override
  Widget build(BuildContext context) {
    var themeTitleLarge = Theme.of(context).textTheme.titleLarge;
    final bool hasThemeTitleLarge = themeTitleLarge != null;

    return Card(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              expense.title,
              style: hasThemeTitleLarge
                  ? themeTitleLarge.copyWith(fontWeight: FontWeight.w700)
                  : Theme.of(context).textTheme.titleLarge,
            ),
            const SizedBox(height: 4.0),
            Row(
              children: <Widget>[
                Text('\$ ${expense.amount.toStringAsFixed(2)}'),
                const Spacer(),
                Row(
                  children: <Widget>[
                    Icon(categoryIcons[expense.category]),
                    const SizedBox(width: 8.0),
                    Text(expense.formattedDate),
                    // Text(expense.category.toString()),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
