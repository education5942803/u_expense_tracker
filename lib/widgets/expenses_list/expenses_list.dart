import 'package:flutter/material.dart';

import '../../models/model_expense.dart';
import 'expense_item.dart';

class ExpensesList extends StatelessWidget {
  const ExpensesList({
    super.key,
    required this.expenses,
    required this.onRemoveExpense,
  });

  final List<ModelExpense> expenses;
  final void Function(ModelExpense expense) onRemoveExpense;

  @override
  Widget build(BuildContext context) {
    var themeMargins = Theme.of(context).cardTheme.margin;
    final bool hasThemeMargins = themeMargins != null;

    return ListView.builder(
      itemCount: expenses.length,
      itemBuilder: (_, index) => Dismissible(
        key: ValueKey(expenses[index].id),
        background: Container(
          color: Theme.of(context).colorScheme.error.withOpacity(0.75),
          margin: hasThemeMargins
              ? EdgeInsets.symmetric(horizontal: themeMargins.horizontal)
              : Theme.of(context).cardTheme.margin,
        ),
        onDismissed: (_) => onRemoveExpense(expenses[index]),
        child: ExpenseItem(expenses[index]),
      ),
    );
  }
}
