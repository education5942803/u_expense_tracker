import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../models/model_expense.dart';

final formatter = DateFormat.yMd();

class NewExpense extends StatefulWidget {
  const NewExpense({
    super.key,
    required this.onAddExpense,
  });

  final void Function(ModelExpense expense) onAddExpense;

  @override
  State<NewExpense> createState() => _NewExpenseState();
}

class _NewExpenseState extends State<NewExpense> {
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _amountController = TextEditingController();
  DateTime? _pickedDate;
  ModelExpenseCategory _pickedCategory = ModelExpenseCategory.work;

  void _openDatePicker() async {
    final DateTime now = DateTime.now();
    final DateTime firstDate = DateTime(now.year - 1, now.month, now.day);
    final DateTime lastDate = DateTime(now.year + 1, now.month, now.day);

    final pickedDate = await showDatePicker(
      context: context,
      initialDate: now,
      firstDate: firstDate,
      lastDate: lastDate,
    );

    setState(() => _pickedDate = pickedDate);
  }

  void _showAlertDialog() {
    if (Platform.isAndroid) {
      showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          title: const Text('invalid input'),
          content: const Text('try input a valid data'),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.pop(ctx),
              child: const Text('Ok'),
            ),
          ],
        ),
      );
    } else {
      showCupertinoDialog(
        context: context,
        builder: (ctx) => CupertinoAlertDialog(
          title: const Text('invalid input'),
          content: const Text('try input a valid data'),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.pop(ctx),
              child: const Text('Ok'),
            ),
          ],
        ),
      );
    }
  }

  void _submitExpenseData() {
    final double? inputAmount = double.tryParse(_amountController.text);

    final bool isTitleInvalid = _titleController.text.trim().isEmpty;
    final bool isAmountInvalid = inputAmount == null || inputAmount <= 0;
    final bool notDatePicked = _pickedDate == null;

    if (isTitleInvalid || isAmountInvalid || notDatePicked) {
      _showAlertDialog();
      return;
    }

    widget.onAddExpense(
      ModelExpense(
        title: _titleController.text,
        amount: inputAmount,
        date: _pickedDate!,
        category: _pickedCategory,
      ),
    );
    Navigator.pop(context);
  }

  @override
  void dispose() {
    _titleController.dispose();
    _amountController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    const double neSheetCommonGap = 16.0;

    final double maxDeviceHeight = MediaQuery.of(context).size.height;
    final double keyboardSpace = MediaQuery.of(context).viewInsets.bottom;
    final double safeAreaTopGap =
        MediaQueryData.fromView(View.of(context)).padding.top;

    final double neSheetHeightWithKbSpace =
        maxDeviceHeight - safeAreaTopGap - keyboardSpace - neSheetCommonGap * 2;
    final double neSheetHeightWoutKbSpace =
        maxDeviceHeight - safeAreaTopGap - neSheetCommonGap * 2;

    final TextField neTitleInputWidget = TextField(
      controller: _titleController,
      maxLength: 50,
      decoration: const InputDecoration(
        label: Text('Title'),
      ),
    );
    final TextField neAmountInputWidget = TextField(
      controller: _amountController,
      maxLength: 10,
      keyboardType: TextInputType.number,
      decoration: const InputDecoration(
        prefixText: '\$ ',
        label: Text('Expense amount'),
      ),
    );
    final DropdownButton neDropdownBtnWidget = DropdownButton(
      value: _pickedCategory,
      items: ModelExpenseCategory.values
          .map(
            (category) => DropdownMenuItem(
              value: category,
              child: Text(category.name),
            ),
          )
          .toList(),
      onChanged: (value) {
        if (value == null) return;
        setState(() => _pickedCategory = value);
      },
    );

    return LayoutBuilder(
      builder: (_, constraints) {
        final maxLayoutWidth = constraints.maxWidth;
        final bool isPortraitView = maxLayoutWidth <= 600;

        return SingleChildScrollView(
          padding: EdgeInsets.fromLTRB(
            neSheetCommonGap,
            neSheetCommonGap,
            neSheetCommonGap,
            keyboardSpace + neSheetCommonGap,
          ),
          child: SizedBox(
            width: MediaQuery.of(context).size.width,
            height: isPortraitView
                ? neSheetHeightWithKbSpace
                : neSheetHeightWoutKbSpace,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                if (isPortraitView)
                  neTitleInputWidget
                else
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Expanded(child: neTitleInputWidget),
                      const SizedBox(width: 24.0),
                      Expanded(child: neAmountInputWidget),
                    ],
                  ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    if (isPortraitView)
                      Expanded(child: neAmountInputWidget)
                    else
                      neDropdownBtnWidget,
                    const SizedBox(width: 20.0),
                    Expanded(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            _pickedDate == null
                                ? 'No date picked'
                                : formatter.format(_pickedDate!),
                          ),
                          IconButton(
                            onPressed: _openDatePicker,
                            icon: const Icon(Icons.calendar_month),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                const SizedBox(height: 20.0),
                if (isPortraitView) neDropdownBtnWidget,
                const Spacer(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    TextButton(
                      onPressed: () => Navigator.pop(context),
                      child: const Text('Cancel'),
                    ),
                    ElevatedButton(
                      onPressed: _submitExpenseData,
                      child: const Text('Save expense'),
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
